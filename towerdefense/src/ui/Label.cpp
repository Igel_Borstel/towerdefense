/*
Copyright (c) 2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

*/
#include "../../include/ui/Label.hpp"

#include <stdexcept>

namespace td
{
	namespace ui
	{
		Label::Label(sf::Vector2i pos, sf::String str, sf::Font f)
			: Widget(pos, 0, 0, str), font(f), text(string, font, 12)
		{
			height = text.getLocalBounds().height;
			width = text.getLocalBounds().width;
			text.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
		}

		Label::Label(sf::Vector2i pos, sf::String str, std::string fontfile)
			: Widget(pos, 0, 0, str)
		{
			if (font.loadFromFile(fontfile))
			{
				text.setString(string);
				text.setFont(font);
				text.setCharacterSize(12);
				height = text.getLocalBounds().height;
				width = text.getLocalBounds().width;
				text.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
			}
			else
			{
				throw std::runtime_error("Could not load font: " + fontfile);
			}
		}

		void Label::handleEvent(sf::Event& const event, sf::Window& const window)
		{
			//no eventhandling needed
		}

		void Label::draw(sf::RenderTarget& const rt)
		{
			if (text.getGlobalBounds().width != width || text.getGlobalBounds().height != height)
			{
				scale();
			}
			if (visible)
			{
				rt.draw(text);
			}
		}

		void Label::scale()
		{
			// Set the new text height
			text.setCharacterSize(static_cast<unsigned int>(height));

			// Get the size of the text
			sf::FloatRect bounds = text.getLocalBounds();

			// The text still has too fit inside the area
			if (bounds.width > width)
			{
				// Adjust the text size
				text.setCharacterSize(static_cast<unsigned int>((height * width) / bounds.width));
			}
		}

		void Label::setPosition(sf::Vector2i pos)
		{
			position = pos;
			text.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
		}

		void Label::move(sf::Vector2i offset)
		{
			position.x += offset.x;
			position.y += offset.y;
			text.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
		}
	}
}