/*
Copyright (c) 2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

*/

#include "../../include/ui/Button.hpp"
#include <stdexcept>

namespace td
{
	namespace ui
	{
		void Button::scale()
		{
			shape.setScale(width / shape.getGlobalBounds().width, height / shape.getGlobalBounds().height);
			// Set the new text height
			text.setCharacterSize(static_cast<unsigned int>(height));

			// Get the size of the text
			sf::FloatRect bounds = text.getLocalBounds();

			// The text still has too fit inside the area
			if (bounds.width > width)
			{
				// Adjust the text size
				text.setCharacterSize(static_cast<unsigned int>((height * width) / bounds.width));
			}
		}

		Button::Button(sf::Vector2i pos, sf::String str, sf::Font f, int w, int h, std::function<void()> func)
			: Widget(pos, w, h, str), font(f), text(string, font), callback(func), shape(sf::Vector2f(width, height))
		{
			while (text.getLocalBounds().height >= height || text.getLocalBounds().width >= width)
			{
				text.setCharacterSize(text.getCharacterSize() - 1);
			}
			shape.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
			text.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
		}

		Button::Button(sf::Vector2i pos, sf::String str, std::string fontfile, int w, int h, std::function<void()> func)
			: Widget(pos, w, h, str), callback(func), shape(sf::Vector2f(width, height))
		{
			if (font.loadFromFile(fontfile))
			{
				text.setString(string);
				text.setFont(font);
				while (text.getLocalBounds().height >= height || text.getLocalBounds().width >= width)
				{
					text.setCharacterSize(text.getCharacterSize() - 1);
				}
				shape.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
				text.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
			}
			else
			{
				throw std::runtime_error("Could not load font: " + fontfile);
			}
		}

		void Button::draw(sf::RenderTarget& const rt)
		{
			if (shape.getGlobalBounds().width != width || shape.getGlobalBounds().height != height)
			{
				scale();
			}
			if (visible)
			{
				if (!textureUsage)
				{
					shape.setFillColor(defaultColor);
				}
				if (hover)
				{
					shape.setFillColor(sf::Color{ static_cast<sf::Uint8>(shape.getFillColor().r * hoverModifier), static_cast<sf::Uint8>(shape.getFillColor().g * hoverModifier), static_cast<sf::Uint8>(shape.getFillColor().b * hoverModifier) });
				}
				if (active)
				{
					shape.setFillColor(sf::Color{ static_cast<sf::Uint8>(shape.getFillColor().r * activeModifier), static_cast<sf::Uint8>(shape.getFillColor().g * activeModifier), static_cast<sf::Uint8>(shape.getFillColor().b * activeModifier) });
				}
				rt.draw(shape);
				rt.draw(text);
			}
		}

		void Button::handleEvent(sf::Event& const event, sf::Window& const window)
		{
			switch (event.type)
			{
			case sf::Event::MouseButtonPressed:
				if (isOnWidget(sf::Mouse::getPosition(window)))
				{
					hover = true;
					if (event.mouseButton.button == sf::Mouse::Button::Left)
					{
						active = true;
					}
				}
				break;
			case sf::Event::MouseButtonReleased:
				if (active)
				{
					active = false;
					callback();
				}
				break;
			case sf::Event::MouseMoved:
				if (isOnWidget(sf::Mouse::getPosition(window)))
				{
					hover = true;
				}
				else
				{
					hover = false;
				}
				break;
			}
		}

		void Button::setUseTexture(bool use) 
		{
			textureUsage = use; 
			if (texture.getSize() == sf::Vector2u(0, 0))
			{
				throw std::runtime_error("Enabled textureusage with no loaded texture!");
			}
		}

		void Button::loadTexture(std::string file)
		{
			if (texture.loadFromFile(file))
			{
				if (textureUsage)
				{
					shape.setTexture(&texture);
				}
			}
			else
			{
				throw std::runtime_error("Could not load texturefile: " + file);
			}
		}

		void Button::loadFont(std::string file)
		{
			if (font.loadFromFile(file))
			{
				text.setFont(font);
			}
			else
			{
				throw std::runtime_error("Could not load font:  " + file);
			}
		}

		void Button::setTexture(sf::Texture& const text) 
		{
			texture = text;
			if (textureUsage)
			{
				shape.setTexture(&texture);
			}
		}

		void Button::setPosition(sf::Vector2i pos)
		{
			position = pos;
			shape.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
			text.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
		}

		void Button::move(sf::Vector2i offset)
		{
			position.x += offset.x;
			position.y += offset.y;
			shape.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
			text.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));
		}
	}
}