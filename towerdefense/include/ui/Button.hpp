/*
Copyright (c) 2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

*/
#ifndef INCLUDE_UI_BUTTON_HPP
#define INCLUDE_UI_BUTTON_HPP

#include "Widget.hpp"
#include <functional>

namespace td
{
	namespace ui
	{
		class Button : public Widget
		{
		protected:
			bool hover{ false };
			bool active{ false };
			bool textureUsage{ false };
			float hoverModifier{ 0.8f };
			float activeModifier{ 0.6f };
			sf::Color defaultColor{ 255, 216, 0, 255 };

			std::function<void()> callback;

			sf::Texture texture;
			sf::RectangleShape shape;
			sf::Text text;
			sf::Font font;
			virtual void scale();
		public:
			Button(sf::Vector2i, sf::String, sf::Font, int, int, std::function<void()>);
			Button(sf::Vector2i, sf::String, std::string, int, int, std::function<void()>);
			virtual ~Button() = default;
			virtual void handleEvent(sf::Event& const, sf::Window& const) override;
			virtual void draw(sf::RenderTarget& const) override;

			virtual bool useTexture() const;
			virtual void setUseTexture(bool = true);
			virtual void setTexture(sf::Texture& const);
			virtual void loadTexture(std::string);
			virtual void setFont(sf::Font& const);
			virtual void loadFont(std::string);
			virtual void setCallback(std::function<void()>);
			virtual void setDefaultColor(sf::Color);
			virtual void setHoverModifier(float);
			virtual void setActiveModifier(float);
			virtual sf::Color getDefaultColor() const;
			virtual float getHoverModifier() const;
			virtual float getActiveModifier() const;
			virtual void setPosition(sf::Vector2i) override;
			virtual void move(sf::Vector2i) override;
		};

		inline bool Button::useTexture() const { return textureUsage; }
		inline sf::Color Button::getDefaultColor() const { return defaultColor; }
		inline float Button::getHoverModifier() const { return hoverModifier; }
		inline float Button::getActiveModifier() const { return activeModifier; }
		inline void Button::setFont(sf::Font& const fnt) { font = fnt; }
		inline void Button::setCallback(std::function<void()> func) { callback = func; }
		inline void Button::setDefaultColor(sf::Color color) { defaultColor = color; }
		inline void Button::setHoverModifier(float mod) { hoverModifier = mod; }
		inline void Button::setActiveModifier(float mod) { activeModifier = mod; }

	}
}

#endif //INCLUDE_UI_BUTTON_HPP