/*
Copyright (c) 2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

*/
#ifndef INCLUDE_UI_WIDGET_HPP
#define INCLUDE_UI_WIDGET_HPP

#include <SFML/Graphics.hpp>
#include <string>

namespace td
{
	namespace ui
	{
		class Widget
		{
			
		protected:
			virtual bool isOnWidget(sf::Vector2i);
			bool visible{ true };
			sf::Vector2i position;
			int width, height;

			sf::String string;
		public:
			Widget(sf::Vector2i, int, int, sf::String);
			virtual ~Widget() = default;
			virtual void handleEvent(sf::Event& const, sf::Window& const) = 0;
			virtual void draw(sf::RenderTarget& const) = 0;
			virtual sf::Vector2i getPosition() const;
			virtual void setPosition(sf::Vector2i);
			virtual bool isVisible() const;
			virtual void setVisible(bool = true);
			virtual sf::IntRect getAllocation() const;
			virtual float getWidth() const;
			virtual float getHight() const;
			virtual void setWidth(int);
			virtual void setHight(int);
			virtual void setAllocation(sf::FloatRect);
			virtual void move(int, int);
			virtual void move(sf::Vector2i);
			virtual sf::String getString() const;
			virtual void setString(sf::String);
		};
		inline sf::Vector2i Widget::getPosition() const { return position; }
		inline void Widget::setPosition(sf::Vector2i pos) { position = pos; }
		inline bool Widget::isVisible() const { return visible; }
		inline void Widget::setVisible(bool vis) { visible = vis; }
		inline sf::IntRect Widget::getAllocation() const { return sf::IntRect(position, sf::Vector2i(width, height)); }
		inline float Widget::getWidth() const { return width; }
		inline float Widget::getHight() const { return height; }
		inline void Widget::setWidth(int w) { width = w; }
		inline void Widget::setHight(int h) {	height = h; }
		inline void Widget::setAllocation(sf::FloatRect allocation)
		{
			position = sf::Vector2i(allocation.left, allocation.top);
			width = allocation.width;
			height = allocation.height;
		}
		inline void Widget::move(int offsetx, int offsety) { move(sf::Vector2i(offsetx, offsety)); }
		inline void Widget::move(sf::Vector2i offset)
		{
			position.x += offset.x;
			position.y += offset.y;
		}
		inline sf::String Widget::getString() const { return string; }
		inline void Widget::setString(sf::String str) { string = str; }

		inline bool Widget::isOnWidget(sf::Vector2i point) { return getAllocation().contains(point); }
	}
}

#endif //INCLUDE_UI_WIDGET_HPP